export interface TableStructureModel {
  Id: number;
  TableTypeId: number;
  Header: string;
  DataType: number|string|Date;
  Format?: string;
  Required: boolean|number;
}
