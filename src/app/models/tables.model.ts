interface TableData1Model{
  Id?: number;
  T1C1: number;
  T1C2: string;
  T1C3: number;
  T1C4: Date|string;
}

interface TableData2Model{
  Id?: number;
  T2C1: number;
  T2C2: string;
  T2C3: number;
  T2C4: Date|string;
  T2C5: number;
}

interface TableData3Model{
  Id?: number;
  T3C1: number;
  T3C2: string;
  T3C3: Date;
}

export { TableData1Model, TableData2Model, TableData3Model };
