import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {pluck} from 'rxjs/operators';
import {TableData1Model, TableData2Model, TableData3Model} from '../../models/tables.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url = 'https://adri-tests-2021.uc.r.appspot.com/api/data/table';

  constructor(
    private http:HttpClient
  ) { }

  getDataTable(id:number){
    return this.http.get(`${this.url}/${id}`).pipe(
      pluck('data')
    )
  }

  insertDataTable(id:string,data:TableData1Model|TableData2Model|TableData3Model){
    return this.http.post(`${this.url}/insert`,{'tableId':id,'data':data})
  }

  updateDataTable(id:string,idTable:string,data:TableData1Model|TableData2Model|TableData3Model){
    return this.http.put(this.url,{'tableId':id,'idDataTable':idTable,'data':data})
  }

  deleteDataTable(id:string,idTable:string){
    return this.http.post(`${this.url}/delete`,{ "tableId":id,'idDataTable':idTable })
  }

}
