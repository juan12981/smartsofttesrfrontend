import { Injectable } from '@angular/core';
import {TableListModel} from '../../models/table-list.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {pluck} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  tableList:TableListModel[];
  url:string = 'https://adri-tests-2021.uc.r.appspot.com/api/config/tables';

  constructor(
    private http:HttpClient,
  ) { }

  getTables():Observable<TableListModel[]>{
    return this.http.get(this.url).pipe(
      pluck('data')
    );
  }

  getTableStructure(id:number){
    return this.http.get(`${this.url}/${id}`).pipe(
      pluck('data')
    )
  }

}
