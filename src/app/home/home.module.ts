import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Modulos usados en los componentes
import { HomeRoutingModule } from './home-routing.module';
import {DropDownListModule} from '@progress/kendo-angular-dropdowns';
import {FormsModule} from '@angular/forms';
import {BodyModule, GridModule, SharedModule} from '@progress/kendo-angular-grid';

//componentes
import { ConfigComponent } from './components/config/config.component';
import { DataComponent } from './components/data/data.component';
import { HomeComponent } from './components/home/home.component';

//servicios
import {ConfigService} from '../services/config/config.service';
import {DataService} from '../services/data/data.service';



@NgModule({
  declarations: [ConfigComponent, DataComponent, HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    DropDownListModule,
    FormsModule,
    BodyModule,
    SharedModule,
    GridModule
  ],
  providers:[
    ConfigService,
    DataService
  ]
})
export class HomeModule { }
