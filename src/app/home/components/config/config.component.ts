import {Component,  EventEmitter, OnInit, Output} from '@angular/core';
import {TableListModel} from '../../../models/table-list.model';
import {ConfigService} from '../../../services/config/config.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.sass']
})

export class ConfigComponent implements OnInit {

  //se crea el output para enviar el dato de la tabla seleccionada
  @Output() sendIdTable = new EventEmitter<TableListModel>();

  tableList: TableListModel[] = [];
  selected: TableListModel;

  constructor(
    private configService:ConfigService
  ){}

  ngOnInit(): void {
    this.getProducts();
    this.sendIdTable.emit({ Id:1,Name:"Tabla 1"});
  }

  //obtiene la lista de tablas de la base de datos
  getProducts(){
    this.configService.getTables().subscribe(
      list => {
        this.tableList = list
        this.selected = list[0]
      }
    )
  }

  //emite el id de la tabla seleccionada en el dropdown
  cargarTabla(ev:TableListModel){
    this.sendIdTable.emit(ev);
  }

}
