import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

import {TableListModel} from '../../../models/table-list.model';
import {TableStructureModel} from '../../../models/table-structure.model';
import {TableData1Model, TableData2Model, TableData3Model} from '../../../models/tables.model';
import {State} from '@progress/kendo-data-query';

import {ConfigService} from '../../../services/config/config.service';
import {DataService} from '../../../services/data/data.service';

import {forkJoin} from 'rxjs';



@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.sass']
})
export class DataComponent implements OnInit,OnChanges{

  //input que recibe el id de la tabla
  @Input() recivedIdTable:TableListModel;


  public formGroup:FormGroup;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  }
  public columns:TableStructureModel[];
  public data:[];

  constructor(
    private configService:ConfigService,
    private dataService:DataService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.getTableStructure(changes.recivedIdTable.currentValue.Id);
  }

  ngOnInit(): void {
  }

  //método para cargar la estructura de las tablas
  getTableStructure(tableId:number){

    const tableStructure$ = this.configService.getTableStructure(tableId);
    const tableData$ = this.dataService.getDataTable(tableId);

    const requestTable$ = forkJoin({
      tableStructure$,
      tableData$
    });

    requestTable$.subscribe(
      response => {
        this.buildResponse(response);
      }
    );
  }

  //método para tratar los datos que se reciben de datTableStructure
  buildResponse(response) {
    this.data = (response.tableData$.length > 0) ? response.tableData$ : [];
    this.columns = response.tableStructure$[0].columns;
  }

  /**
   * Metodos KendoUI para el manejo de la tabla
   */

  cancelHandler({sender, rowIndex}) {
    // close the editor for the given row
    sender.closeRow(rowIndex)
  }

  saveHandler({sender, rowIndex, formGroup, isNew}) {
    let register:TableData1Model|TableData2Model|TableData3Model = formGroup.value;
    if(isNew){
      this.dataService.insertDataTable(this.recivedIdTable.Id.toString(),register).subscribe(
        () => this.getTableStructure(this.recivedIdTable.Id),
        ()=> console.log
      )
    }else{
      this.dataService.updateDataTable(this.recivedIdTable.Id.toString(),formGroup.value.Id.toString(),register).subscribe(
        () => this.getTableStructure(this.recivedIdTable.Id),
        ()=> console.log
      )
    }
    sender.closeRow(rowIndex);
  }

  removeHandler({dataItem}) {
    this.dataService.deleteDataTable(this.recivedIdTable.Id.toString(),dataItem.Id.toString()).subscribe(
      () => this.getTableStructure(this.recivedIdTable.Id),
      () => console.log
    )
  }

  editHandler({sender, rowIndex, dataItem}) {
    // define all editable fields validators and default values
    let group;
    switch (this.recivedIdTable.Id) {
      case 1:
        group = new FormGroup({
          'Id':new FormControl(dataItem.Id),
          'T1C1':new FormControl(dataItem.T1C1),
          'T1C2':new FormControl(dataItem.T1C2),
          'T1C3':new FormControl(dataItem.T1C3),
          'T1C4':new FormControl(dataItem.T1C4),
        });
        break;
      case 2:
        group = new FormGroup({
          'Id':new FormControl(dataItem.Id),
          'T2C1':new FormControl(dataItem.T2C1),
          'T2C2':new FormControl(dataItem.T2C2),
          'T2C3':new FormControl(dataItem.T2C3),
          'T2C4':new FormControl(dataItem.T2C4),
          'T2C5':new FormControl(dataItem.T2C5),
        });
        break;
      case 3:
        group = new FormGroup({
          'Id':new FormControl(dataItem.Id),
          'T3C1':new FormControl(dataItem.T3C1),
          'T3C2':new FormControl(dataItem.T3C2),
          'T3C3':new FormControl(dataItem.T3C3),
        });
        break;
    }
    // put the row in edit mode, with the `FormGroup` build above
    sender.editRow(rowIndex, group);
  }

  addHandler({sender}) {
    let group;
    switch (this.recivedIdTable.Id) {
      case 1:
        group = new FormGroup({
          'Id':new FormControl(),
          'T1C1':new FormControl(),
          'T1C2':new FormControl(),
          'T1C3':new FormControl(),
          'T1C4':new FormControl(),
        });
        break;
      case 2:
        group = new FormGroup({
          'Id':new FormControl(),
          'T2C1':new FormControl(),
          'T2C2':new FormControl(),
          'T2C3':new FormControl(),
          'T2C4':new FormControl(),
          'T2C5':new FormControl(),
        });
        break;
      case 3:
        group = new FormGroup({
          'Id':new FormControl(),
          'T3C1':new FormControl(),
          'T3C2':new FormControl(),
          'T3C3':new FormControl(),
        });
        break;
    }
    // show the new row editor, with the `FormGroup` build above
    sender.addRow(group);
  }

}
