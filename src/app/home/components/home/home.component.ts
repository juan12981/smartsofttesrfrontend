import { Component, OnInit } from '@angular/core';
import {TableListModel} from '../../../models/table-list.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  table:TableListModel;

  constructor() { }

  ngOnInit(): void {
  }

  getTableID(datTable:TableListModel){
    this.table = datTable;
  }

}
